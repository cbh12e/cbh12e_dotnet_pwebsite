﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casey_Leon.Models
{
    public class Navigation
    {

        
        public int NavigationId { get; set; }

        public string NavigationUrl { get; set; }

        public string NavigationName { get; set; }
    }
}
