﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casey_Leon.Models
{
    public class Portfolio
    {


        public int PortfolioId { get; set; }

        public string PortfolioUrl { get; set; }

        public string PortfolioName { get; set; }

        public string PortfolioPhoto { get; set; }
    }
}
