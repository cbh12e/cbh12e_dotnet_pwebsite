﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casey_Leon.Models;
using Microsoft.EntityFrameworkCore;

namespace Casey_Leon.Context
{
    public class LeonDb:DbContext
    {
        public LeonDb(DbContextOptions<LeonDb> options)
            : base(options)
        {
        }
        public DbSet<Portfolio> Portfolios { get; set; }
    }
}
