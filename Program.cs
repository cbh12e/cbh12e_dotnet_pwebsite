﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
//using MySql.Data.MySqlClient;


namespace Casey_Leon
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
          /*  MySqlConnection connection = new MySqlConnection
            {
                ConnectionString = "server=localhost;user id=root;password=;persistsecurityinfo=True;port=3306;database=cbh_portfolio"
            };
            connection.Open();
            */
        }
    }
}
