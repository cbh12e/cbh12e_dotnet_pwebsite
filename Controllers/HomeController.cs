﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
//using Casey_Leon.Context;

namespace Casey_Leon.Controllers
{
  //  private LeonDb webAPIDataContext;
    public class HomeController : Controller
    {

        public HomeController()
        {
            //_webApiDataContext = _webApiDataContext;
            //webApiDataContext.Database.EnsureCreated();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Resume()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Portfolio()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        public IActionResult Error(HttpStatusCode statusCode)
        {
               
            ViewData["Message"] = statusCode;
            return View();
        }
    }
}
